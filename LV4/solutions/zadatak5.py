import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)


np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]


linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)


ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)


x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
#plt.plot(x_pravac, y_pravac)
#plt.show()

stupacJedinica=np.ones(len(xtrain))
stupacJedinica=stupacJedinica[:,np.newaxis]
x_p=np.hstack((stupacJedinica,xtrain))

transp=np.transpose(x_p)
inverz=np.linalg.inv(np.dot(transp,x_p))
theta1=np.dot(np.dot(inverz,transp),ytrain)

n=len(xtrain)
def pravac(x, theta0, theta1):
    return theta0 + theta1*x

alfa = 0.01
theta = [0,0]
sve_udalj=[]

for i in range(0,5000):
    sumatheta1=0
    sumatheta2=0
    udaljenost=0
    for j in range (0, n):
        sumatheta1= sumatheta1 + (pravac(xtrain[j], theta[0], theta[1])-ytrain[j])*xtrain[j]/len(xtrain)
        sumatheta2= sumatheta2 + (pravac(xtrain[j], theta[0], theta[1])-ytrain[j])/len(xtrain);
        udaljenost=udaljenost + abs(ytrain[j]-theta[0]-theta[1]*xtrain[j]) 
    sve_udalj.append(udaljenost) 
    theta[0] =theta[0] - alfa*sumatheta2;    
    theta[1] =theta[1] - alfa*sumatheta1;

suma=0
y_crtica=0
for i in range (0, len(xtrain)):
    suma+=pow((ytrain[i]-theta[0]-theta[1]*xtrain[i]),2)
    y_crtica+=ytrain[i]
mse_train=(1./len(xtrain))*suma; 
y_crtica=y_crtica/len(ytrain)

suma1=0
y_crtica1=0
for i in range (0, len(xtest)):
    suma1+=pow((ytest[i]-theta[0]-theta[1]*xtest[i]),2)
    y_crtica1+=ytest[i]
y_crtica1=y_crtica1/len(ytest)
    
mse_test=(1./len(xtest))*suma1;
    
print ('Srednja kvadratna pogreška na skupu za učenje:',mse_train)
print ('Srednja kvadratna pogreška na skupu za testiranje:',mse_test)

nazivnik=0
for i in range (0, len(ytrain)):
    nazivnik+= (ytrain[i]- y_crtica)
R2=1.-(suma/nazivnik)
print ('Koeficijent determinacije na skupu za učenje',R2)

nazivnik1=0
for i in range (0, len(ytest)):
    nazivnik1+= (ytest[i]- y_crtica1)
R2=1.-(suma1/nazivnik1)

print ('Koeficijent determinacije na skupu za testiranje',R2)