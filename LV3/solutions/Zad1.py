import pandas as pd 

#1.

mtcars = pd.read_csv('mtcars.csv')
maxPotrosaci = mtcars.sort_values(by=['mpg'], ascending=True) 
print ("Najveci potrosaci: ")
print (maxPotrosaci['car'].head(5), "\n")


#2.
min8cilindar= mtcars[mtcars.cyl == 8] 
sortedcilindar8 = min8cilindar.sort_values(by=['mpg'], ascending=False)
print ("Najmanji potrosaci sa 8 cilindra: ")
print (sortedcilindar8['car'].head(3), "\n")

#3.

avg6cilindar = mtcars[mtcars.cyl == 6]
srednjaPotrosnja = avg6cilindar["mpg"].mean()
print("Srednja potrošnja auta sa 6 cilindra: ")
print(srednjaPotrosnja, "\n")

#4.

avg4cilindar = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2) & (mtcars.wt <=2.2)]
srednjaPotrosnja4 = avg4cilindar["mpg"].mean()
print ("Srednja potrošnja auta sa 4 cilindra: ")
print (srednjaPotrosnja4, "\n")

#5.

rucniMjenjac = mtcars.groupby('am').gear
print ("Rucni mjenjac = 0, Automaski mjenjac  = 1")
print (rucniMjenjac.count(), '\n')

#6.

automatskiMjenjac = mtcars[(mtcars.am == 1) & (mtcars.hp >= 100)]
print("Auti sa automatskim mjenjacem i više od 100 konjskih snaga: ") 
print(automatskiMjenjac.count().gear, '\n')

#7.

lb = 0.45359
a = 0
for i in mtcars["wt"]:
    redak = i * 1000
    redak = redak * lb
    a = a+1
    print ("%d. auto : %.3f kg" %(a, redak))