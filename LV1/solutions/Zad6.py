fileName = input("Unesite put do datoteke: ")
try:
    fhand = open(fileName)
except:
    print ('File cannot be opened: ', fileName)
    exit()

emails = []
domains = []
dictOfDomains = {}

for line in fhand:
    if(line.startswith('From')):
        email = line.split(" ")[1].strip()
        emails.append(email)
        domain = email.split("@")[1]
        domains.append(domain)

for domain in domains:
    dictOfDomains[domain] = dictOfDomains.get(domain, 0) + 1

print("Nekoliko email adresa: ", emails[:4])
print("Nekoliko domena : ", dictOfDomains)
