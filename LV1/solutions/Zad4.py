list = []
while(1):
    input_ = input("Unesite broj ili \"Done\" za kraj:  ")
    if input_ == "Done":
        break
    try:
        list.append(float(input_))
    except:
        print("Not a number")
count = len(list)
min = min(list)
max = max(list)
avg = sum(list)/count
print(
    '''\tBroj unosa: {}
\tMinimalni: {}
\tMaksimalni: {}
\tSrednja vrijednost: {}'''.format(count, min, max, avg ))  